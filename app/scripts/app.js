'use strict';

/**
 * @ngdoc overview
 * @name angularFacturacionApp
 * @description
 * # angularFacturacionApp
 *
 * Main module of the application.
 */
angular
  .module('angularFacturacionApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'main',
        controllerAs: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/ventas', {
        templateUrl: 'views/ventas.html',
        controller: 'VentasController',
        controllerAs: 'ventas'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
