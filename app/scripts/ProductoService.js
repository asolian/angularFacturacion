/**
 * Created by auxeva on 14/06/16.
 */
var app = angular.module('angularFacturacionApp');

app.factory('productoService',function($http){

    function lista(callback) {
        $http.post('http://localhost')
            .success(function (data) {
                callback(data);
            });
    };
    function detallePos(pos) {
        return productos[pos];
    }

    return {
        lista : lista,
        detallePos:detallePos
    };
});

/*
app.factory('productoService',function(){
    var productos = [{'nombre':'Playstation 4','precio':'300'},
        {'nombre':'Doom','precio':'60'},
        {'nombre':'The Witcher 3','precio':'50'},
        {'nombre':'Shadow','precio':'15'},
        {'nombre':'Ratchet','precio':'35'},
        {'nombre':'Age of empires 2','precio':'25'},
        {'nombre':'The Witcher 2','precio':'10'},
        {'nombre':'Uncharted 4','precio':'60'}];
    function lista() {
        return productos;
    }
    function detallePos(pos) {
        return productos[pos];
    }

    return {
        lista : lista,
        detallePos:detallePos
    };
});*/