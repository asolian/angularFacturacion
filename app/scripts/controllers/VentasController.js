'use strict';

/**
 * @ngdoc function
 * @name angularFacturacionApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularFacturacionApp
 */
angular.module('angularFacturacionApp')
  .controller('VentasController', function ($scope, productoService) {
    $scope.totalVentas = 0;
    $scope.totalItems = 0;
    $scope.productosVenta = new Array();
    productoService.lista(function (prod) {
                $scope.productosStock = prod;
    });
      /*
       $scope.productosStock = productoService.lista();

      $scope.productosStock = [{'nombre':'Playstation 4','precio':'300'},
                            {'nombre':'Doom','precio':'60'},
                            {'nombre':'The Witcher 3','precio':'50'},
                            {'nombre':'Shadow','precio':'15'},
                            {'nombre':'Ratchet','precio':'35'},
                            {'nombre':'Age of empires 2','precio':'25'},
                            {'nombre':'The Witcher 2','precio':'10'},
                            {'nombre':'Uncharted 4','precio':'60'}];
      */
    $scope.agregar = function(p){
      if($scope.productosVenta.indexOf(p) == -1)
      {
        p.cantidad = 1;
        $scope.productosVenta.push(p);
      }else {
        $scope.productosVenta[$scope.productosVenta.indexOf(p)].cantidad += 1;
      };
      $scope.totalItems += 1;
      $scope.totalVentas += parseFloat(p.precio);
    };
  });
