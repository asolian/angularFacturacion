'use strict';

/**
 * @ngdoc function
 * @name angularFacturacionApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularFacturacionApp
 */
angular.module('angularFacturacionApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
